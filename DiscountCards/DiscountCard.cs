﻿namespace DiscountCards
{
    internal abstract class DiscountCard
    {
        public string OwnerName { get; set; }

        public decimal InitialDiscountRate { get; set; }

        public decimal Turnover { get; set; }

        protected DiscountCard(string name, decimal turnover)
        {
            OwnerName = name;
            Turnover = turnover;
            InitialDiscountRate = SetInitialDiscountRate(Turnover);
        }

        public virtual decimal CalculateDiscountValue(decimal purchaseValue)
        {
            return purchaseValue * InitialDiscountRate;
        }

        protected abstract decimal SetInitialDiscountRate(decimal turnover);
    }
}
