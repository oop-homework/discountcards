﻿namespace DiscountCards
{
    internal class SilverCard : DiscountCard
    {
        public SilverCard(string name, decimal turnover) 
            : base(name, turnover)
        {
            
        }

        protected override decimal SetInitialDiscountRate(decimal turnover)
        {
            return turnover > 300 ? 0.035m : 0.02m;
        }
    }
}
