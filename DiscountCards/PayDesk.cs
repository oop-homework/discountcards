﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DiscountCards
{
    internal class PayDesk
    {
        public static void PrintCardInfo(DiscountCard card, decimal purchaseValue)
        {
            decimal discountValue = card.CalculateDiscountValue(purchaseValue);
            Console.WriteLine($"Purchase value: ${purchaseValue}");
            Console.WriteLine($"Discount rate: {card.InitialDiscountRate * 100:F2}%");
            Console.WriteLine($"Discount value: ${discountValue:F2}");
            Console.WriteLine($"Total: ${purchaseValue - discountValue:F2}");
        }
    }
}
