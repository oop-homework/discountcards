﻿namespace DiscountCards
{
    internal class GoldCard : DiscountCard
    {
        public GoldCard(string name, decimal turnover) 
            : base(name, turnover)
        {
            
        }

        protected override decimal SetInitialDiscountRate(decimal turnover)
        {
            if (turnover < 100)
            {
                return 0.02m;
            }

            decimal calculatedDiscountRate = (2 + (Turnover / 100)) / 100;
            return calculatedDiscountRate > 0.1m ? 0.1m : calculatedDiscountRate;
        }
    }
}
