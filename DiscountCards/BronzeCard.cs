﻿namespace DiscountCards
{
    internal class BronzeCard : DiscountCard
    {
        public BronzeCard(string name, decimal turnover) 
            : base(name, turnover)
        {
            
        }

        protected override decimal SetInitialDiscountRate(decimal turnover)
        {
            if (Turnover < 100)
            {
                return 0;
            }

            if (Turnover < 300)
            {
                return 0.01m;
            }

            return 0.025m;
        }
    }
}
