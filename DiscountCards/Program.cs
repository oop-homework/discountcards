﻿// See https://aka.ms/new-console-template for more information
using DiscountCards;

BronzeCard bronzeCard = new BronzeCard("Bronze owner", 0);
SilverCard silverCard = new SilverCard("Silver owner", 600);
GoldCard goldCard = new GoldCard("Gold owner", 1500);

Console.WriteLine("Bronze");
PayDesk.PrintCardInfo(bronzeCard, 150);

Console.WriteLine("Silver");
PayDesk.PrintCardInfo(silverCard, 850);

Console.WriteLine("Gold");
PayDesk.PrintCardInfo(goldCard, 1300);
